LOCAL_PATH := $(call my-dir)

TEGRAFLASH_PATH := $(BUILD_TOP)/vendor/nvidia/common/tegraflash
T234_BL         := $(BUILD_TOP)/vendor/nvidia/t234/bootloader
CONCORD_BCT     := $(BUILD_TOP)/vendor/nvidia/concord/BCT
CONCORD_FLASH   := $(BUILD_TOP)/device/nvidia/concord/flash_package
COMMON_FLASH    := $(BUILD_TOP)/device/nvidia/tegra-common/flash_package

TNSPEC_PY    := $(BUILD_TOP)/vendor/nvidia/common/tegraflash/tnspec.py

INSTALLED_KERNEL_TARGET        := $(PRODUCT_OUT)/kernel
INSTALLED_RECOVERYIMAGE_TARGET := $(PRODUCT_OUT)/recovery.img
INSTALLED_TOS_TARGET           := $(PRODUCT_OUT)/tos-mon-only.img

TOYBOX_HOST  := $(HOST_OUT_EXECUTABLES)/toybox
AWK_HOST     := $(HOST_OUT_EXECUTABLES)/one-true-awk
AVBTOOL_HOST := $(HOST_OUT_EXECUTABLES)/avbtool

include $(CLEAR_VARS)
LOCAL_MODULE        := p3710_flash_package
LOCAL_MODULE_SUFFIX := .txz
LOCAL_MODULE_CLASS  := ETC
LOCAL_MODULE_PATH   := $(PRODUCT_OUT)

_p3710_package_intermediates := $(call intermediates-dir-for,$(LOCAL_MODULE_CLASS),$(LOCAL_MODULE))
_p3710_package_archive := $(_p3710_package_intermediates)/$(LOCAL_MODULE)$(LOCAL_MODULE_SUFFIX)

$(_p3710_package_archive): $(INSTALLED_KERNEL_TARGET) $(INSTALLED_RECOVERYIMAGE_TARGET) $(INSTALLED_TOS_TARGET) $(AWK_HOST) $(TOYBOX_HOST) $(AVBTOOL_HOST)
	@mkdir -p $(dir $@)/tegraflash
	@mkdir -p $(dir $@)/scripts
	@cp $(TEGRAFLASH_PATH)/tegraflash* $(dir $@)/tegraflash/
	@cp $(TEGRAFLASH_PATH)/*_v2 $(dir $@)/tegraflash/
	@cp $(TEGRAFLASH_PATH)/tegraopenssl $(dir $@)/tegraflash/
	@cp $(TEGRAFLASH_PATH)/tegrasign_v3* $(dir $@)/tegraflash/
	@cp $(TEGRAFLASH_PATH)/sw_memcfg_overlay.pl $(dir $@)/tegraflash/
	@cp -R $(TEGRAFLASH_PATH)/pyfdt $(dir $@)/tegraflash/
	@cp $(COMMON_FLASH)/*.sh $(dir $@)/scripts/
	@cp $(GALEN_FLASH)/p3710.sh $(dir $@)/flash.sh
	@cp $(GALEN_FLASH)/flash_android_t234_sdmmc.xml $(dir $@)/
	@cp $(T194_BL)/* $(dir $@)/
	@cp $(INSTALLED_TOS_TARGET) $(dir $@)/tos-mon-only_t234.img
	@$(AVBTOOL_HOST) make_vbmeta_image --flags 2 --padding_size 256 --output $(dir $@)/vbmeta_skip.img
	@cp $(INSTALLED_RECOVERYIMAGE_TARGET) $(dir $@)/
	@cp $(KERNEL_OUT)/arch/arm64/boot/dts/nvidia/tegra234-p3701-0000-p3737-0000.dtb $(dir $@)/
	@cp $(GALEN_BCT)/* $(dir $@)/
	@echo "NV4" > $(dir $@)/qspi_bootblob_ver.txt
	@echo "# R19 , REVISION: 1" >> $(dir $@)/qspi_bootblob_ver.txt
	@echo "BOARDID= BOARDSKU= FAB=" >> $(dir $@)/qspi_bootblob_ver.txt
	@$(TOYBOX_HOST) date '+%Y%m%d%H%M%S' >> $(dir $@)/qspi_bootblob_ver.txt
	@echo "0x230100" >> $(dir $@)/qspi_bootblob_ver.txt
	@$(TOYBOX_HOST) cksum $(dir $@)/qspi_bootblob_ver.txt |$(AWK_HOST) '{ print "BYTES:" $$2, "CRC32:" $$1 }' >> $(dir $@)/qspi_bootblob_ver.txt
	@cd $(dir $@); tar -cJf $(abspath $@) *

include $(BUILD_SYSTEM)/base_rules.mk
